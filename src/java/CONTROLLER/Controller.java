/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import GESTION.GestionFrns;
import MODEL.Fournisseur;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tsiry
 */
public class Controller extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    String  Liste_Frns="VIEW/Fournisseur/Liste_Frns.jsp";
    String Add_Frns="VIEW/Fournisseur/Add_Frns.jsp";
    String Edit_Frns="VIEW/Fournisseur/Edit_Frns.jsp";
    String index="JSPMVC/index.jsp";
    GestionFrns gf = new GestionFrns();
    Fournisseur f=new Fournisseur();
    
        
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
           String acces="";
           String action=request.getParameter("act");
           if(action.equalsIgnoreCase("liste_Frns")){
                acces = Liste_Frns;  
           }
           else if(action.equalsIgnoreCase("index")){
                acces ="";  
           }else if(action.equalsIgnoreCase("Enregistrer fournisseur")){
                String nm = request.getParameter("nomFrns");  
                String tel = request.getParameter("telFrns");
                f.setNomFrns(nm);
                f.setTelFrns(tel);
                gf.add(f);
                acces=Liste_Frns;
           }else if(action.equalsIgnoreCase("Modifier fournisseur")){
                String idEdit =request.getParameter("idEdit"); 
                String nmEdit = request.getParameter("nomEdit");  
                String telEdit = request.getParameter("telEdit");
                f.setIdFrns(Integer.parseInt(idEdit));
                f.setNomFrns(nmEdit);
                f.setTelFrns(telEdit);
                gf.edit(f);
                acces=Liste_Frns;
           }else if(action.equalsIgnoreCase("Supprimer fournisseur")){
                String idSuppr=request.getParameter("idSuppr");   
                gf.sup(Integer.parseInt(idSuppr));
                acces=Liste_Frns;
           }
           
       RequestDispatcher view=request.getRequestDispatcher(acces);
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
