/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Tsiry
 */
import java.sql.*;
public class Connexion {
    Connection con;
    public Connexion(){
        try {
            Class.forName(Const.driv);
            con= DriverManager.getConnection(Const.URL, Const.USER, Const.PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    
    }
    public Connection getCon(){
    return con;
    }
}
