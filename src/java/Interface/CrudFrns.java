/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import MODEL.Fournisseur;
import java.util.List;

/**
 *
 * @author Tsiry
 */
public interface CrudFrns {
   public List lister();
   public Fournisseur list(int id);
   public boolean add(Fournisseur f);
   public boolean edit(Fournisseur f);
   public boolean sup(int id);
    
}
