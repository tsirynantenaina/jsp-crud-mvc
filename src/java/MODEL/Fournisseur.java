/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODEL;

/**
 *
 * @author Tsiry
 */
public class Fournisseur {
    int idFrns ; 
    String nomFrns;
    String telFrns ;
    public Fournisseur(){
}
    public Fournisseur(int idFrns, String nomFrns, String telFrns) {
        this.idFrns = idFrns;
        this.nomFrns = nomFrns;
        this.telFrns = telFrns;
    }

    public Fournisseur(String nomFrns, String telFrns) {
        this.nomFrns = nomFrns;
        this.telFrns = telFrns;
    }

    public int getIdFrns() {
        return idFrns;
    }

    public void setIdFrns(int idFrns) {
        this.idFrns = idFrns;
    }

    public String getNomFrns() {
        return nomFrns;
    }

    public void setNomFrns(String nomFrns) {
        this.nomFrns = nomFrns;
    }

    public String getTelFrns() {
        return telFrns;
    }

    public void setTelFrns(String telFrns) {
        this.telFrns = telFrns;
    }
    
    
}
