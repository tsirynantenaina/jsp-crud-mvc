/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GESTION;

import Config.Connexion;
import Interface.CrudFrns;
import MODEL.Fournisseur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tsiry
 */
public class GestionFrns implements CrudFrns {
    Connexion ccn=new Connexion();
    Connection co;
    PreparedStatement st;
    ResultSet rs;
    Fournisseur f=new Fournisseur ();

    @Override
    public List lister() {
         ArrayList<Fournisseur> list=new ArrayList<>(); 
       String sel="select * from fournisseur";
        try {
            co=ccn.getCon();
            st=co.prepareStatement(sel);
            rs=st.executeQuery();
            while(rs.next()){
            Fournisseur Fr=new Fournisseur();
            Fr.setIdFrns(rs.getInt("idFrns"));
            Fr.setNomFrns(rs.getString("nomFrns"));
            Fr.setTelFrns(rs.getString("TelFrns"));
            list.add(Fr);
            }
        } catch (Exception e) {
        }
       return list;}

    @Override
    public Fournisseur list(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(Fournisseur f) {
    String sql="insert into fournisseur(nomFrns , telFrns)values('"+f.getNomFrns()+"' , '"+f.getTelFrns()+"')";
        try {
            co=ccn.getCon();
            st=co.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return false ;
    }

    @Override
    public boolean edit(Fournisseur f) {
        String sql="update fournisseur set nomFrns ='"+f.getNomFrns()+"' , telFrns ='"+f.getTelFrns()+"' where idFrns= "+f.getIdFrns()+"";
        try {
            co=ccn.getCon();
            st=co.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
        }
       return false;
    }

    @Override
    public boolean sup(int id) {
        String sql="delete from fournisseur where idFrns="+id+"";
       try {
            co=ccn.getCon();
            st=co.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
}
